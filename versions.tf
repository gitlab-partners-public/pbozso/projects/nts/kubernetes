terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
    flux = {
      source  = "fluxcd/flux"
      version = ">= 1.0.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">=15.10.0"
    }
  }
  required_version = ">=1.1.5"
}
